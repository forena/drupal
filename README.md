# Forena Reports

Enterprise Reporting module that provides a mechanism for writing reports 
in a modified html syntax.

**Author** :David Metzler (metzlerd) 
   
## Installation

Install in your sites/all/modules folder per the normal module installation 
method. 

## Documentation
Please refer to the following guides for developing with forena: 

* [FRX Reporting Reference](doc/FrxReportingReference.md)
* [SQL Developer's guide](doc/DataGuide.md)
* [Application Development](doc/DevelopersGuide.md)

Additional information by be found at Forena.org(http://forena.org/reports/help)
